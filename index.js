const express = require("express"),
	bodyParser = require("body-parser");

let scrapperRouter = require("./routes/scrapper");

var app = express();

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

// app.set("views", "./views");
// app.set("view engine", "ejs");

// app.get("/", function(req, res) {
// 	res.render("login", {
// 		loginError: false
// 	});
// });

app.use("/api", scrapperRouter);

app.listen(3000, function() {
	console.log("running as a local machine on port: " + 3000);
});

module.exports = app;
