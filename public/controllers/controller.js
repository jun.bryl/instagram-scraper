var appname = angular.module("app", []);

appname.controller("appCtrl", [
	"$scope",
	"$http",
	function($scope, $http) {
		vm = this;

		vm.getInstaData = getInstaData;
		vm.changeSortOrder = changeSortOrder;
		vm.showDownCaret = showDownCaret;
		vm.showUpCaret = showUpCaret;

		vm.instaHandle = "";

		vm.user = {};
		vm.user.fullName = "";
		vm.user.description = "";
		vm.user.followers = "";
		vm.user.following = "";
		vm.user.recentPosts = [];
		vm.user.isPrivate = true;
		vm.user.isEmpty = true;

		vm.loading = false;
		vm.isLoadingError = false;
		vm.loadingErrorStatus = "";

		vm.sortBy = "";
		vm.isReverseSort = false;

		function getInstaData(instaHandle) {
			initUserData();
			vm.loading = true;
			vm.isLoadingError = false;
			vm.loadingErrorStatus = "";

			$http.post("/api", { instaHandle: instaHandle }).then(
				function success(response) {
					let userData = response.data;

					vm.user.isEmpty = false;
					vm.user.isPrivate = userData.isPrivate;

					if (!vm.user.isPrivate) {
						vm.user.fullName = userData.userInfo.fullName;
						vm.user.description = userData.userInfo.description;
						vm.user.followers = userData.userInfo.followers;
						vm.user.following = userData.userInfo.following;
						vm.user.recentPosts = userData.recentPosts;
					}

					vm.loading = false;
				},
				function error(error) {
					vm.loading = false;
					vm.isLoadingError = true;
					vm.loadingErrorStatus = error.status;
				}
			);
		}

		function initUserData() {
			vm.user.fullName = "";
			vm.user.description = "";
			vm.user.followers = "";
			vm.user.following = "";
			vm.user.recentPosts = [];
			vm.user.isPrivate = false;
			vm.user.isEmpty = true;
			vm.sortBy = "";
			vm.isReverseSort = false;
		}

		function changeSortOrder(name) {
			vm.sortBy = name;
			vm.isReverseSort = !vm.isReverseSort;
		}

		function showDownCaret(name) {
			return vm.sortBy == name && !vm.isReverseSort;
		}
		function showUpCaret(name) {
			return vm.sortBy == name && vm.isReverseSort;
		}
	}
]);
