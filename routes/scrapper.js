const express = require("express");
const request = require("request-promise");
const cheerio = require("cheerio");
const axios = require("axios");

const baseUrl = "https://www.instagram.com/"; // could be moved to an .env file

let scrapperRouter = express.Router();

scrapperRouter.post("/", async function(req, res) {
	try {
		let instaHandle = req.body.instaHandle || "";
		let userData = await getInstaData(instaHandle);
		res.send(userData);
	} catch (err) {
		res.status(404).send("error getting data");
	}
});

scrapperRouter.get("/", async function(req, res) {
	try {
		let userData = await getInstaData("jungitest");
		res.send(userData);
	} catch (err) {
		res.status(404).send("error getting data");
	}
});

async function getInstaData(_instgramHandle) {
	let userData = {};
	let userInfo = {};
	let recentPosts = [];

	const instagramDataURL = baseUrl + _instgramHandle + "/?__a=1";

	let isPrivate = await checkIfPrivateInsta(_instgramHandle);
	if (!isPrivate) {
		let { data: responseData } = await axios.get(instagramDataURL);
		let resUser = responseData.graphql.user;
		let resRecentPosts = resUser.edge_owner_to_timeline_media.edges.slice(0, 6);

		userInfo = {
			fullName: resUser.full_name,
			description: resUser.biography,
			followers: resUser.edge_followed_by.count,
			following: resUser.edge_follow.count
		};

		resRecentPosts.forEach(function(post) {
			let postData = {};
			let node = post.node;
			postData.id = node.id;
			postData.imageUrl = node.display_url;
			postData.caption = "";

			if (node.edge_media_to_caption.edges.length > 0) {
				postData.caption = node.edge_media_to_caption.edges[0].node.text;
			}

			recentPosts.push(postData);
		});
	}

	userData = {
		isPrivate,
		userInfo,
		recentPosts
	};

	return userData;
}

async function checkIfPrivateInsta(_instgramHandle) {
	const instagramProfURL = baseUrl + _instgramHandle + "/";
	const response = await request({
		uri: instagramProfURL,
		headers: {
			accept:
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
			"accept-encoding": "gzip, deflate, br",
			"accept-language": "en-US,en;q=0.9,pt;q=0.8",
			"cache-control": "max-age=0",
			connection: "keep-alive",
			host: "www.instagram.com",
			"upgrade-insecure-requests": "1",
			"user-agent":
				"Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Mobile Safari/537.36"
		},
		gzip: true
	});

	let $ = cheerio.load(response);

	let sharedData = $("body > script")
		.eq(0)
		.html();
	sharedData = sharedData.slice(0, -1); // remove semicolon
	sharedData = sharedData.replace("window._sharedData = ", "");
	let dataJSON = JSON.parse(sharedData);

	let {
		is_private: isPrivate
	} = dataJSON.entry_data.ProfilePage[0].graphql.user;

	return isPrivate;
}

module.exports = scrapperRouter;
